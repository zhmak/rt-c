/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   specular.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdibbert <fdibbert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 18:18:18 by fdibbert          #+#    #+#             */
/*   Updated: 2019/02/23 20:50:02 by fdibbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	object_color(int *color, t_rt *rt, int num)
{
	t_cylinder	*cylinder;
	t_cone		*cone;
	t_plane		*plane;
	t_sphere	*sphere;

	if (rt->objects[num].id == SPHERE)
	{
		sphere = (t_sphere *)rt->objects[num].object;
		*color = sphere->color;
	}
	else if (rt->objects[num].id == PLANE)
	{
		plane = (t_plane *)rt->objects[num].object;
		*color = plane->color;
	}
	else if (rt->objects[num].id == CYLINDER)
	{
		cylinder = (t_cylinder *)rt->objects[num].object;
		*color = cylinder->color;
	}
	else if (rt->objects[num].id == CONE)
	{
		cone = (t_cone *)rt->objects[num].object;
		*color = cone->color;
	}
}
