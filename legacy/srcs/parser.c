/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 14:38:08 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 15:52:31 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void	parse_light(char **params, t_rt *rt)
{
	char	**coords;
	t_vec3	*light;

	*(rt->light_sources) = (t_vec3 *)malloc(sizeof(t_vec3));
	coords = ft_strsplit(params[1], ',');
	light = *(rt->light_sources);
	light->x = ft_atof(coords[0]);
	light->y = ft_atof(coords[1]);
	light->z = ft_atof(coords[2]);
	rt->light_sources++;
	free_words(coords);
	free_words(params);
}

static void	parse_cam(char **params, t_rt *rt)
{
	char	**coords;

	coords = ft_strsplit(params[1], ',');
	rt->cam.x = ft_atof(coords[0]);
	rt->cam.y = ft_atof(coords[1]);
	rt->cam.z = ft_atof(coords[2]);
	free_words(coords);
	coords = ft_strsplit(params[2], ',');
	rt->view_port.angle_x = ft_atof(coords[0]);
	rt->view_port.angle_y = ft_atof(coords[1]);
	free_words(coords);
	free(params);
}

static void	select_function(t_rt *rt, char **params)
{
	if (ft_strcmp(params[0], "camera") == 0)
		parse_cam(params, rt);
	else if (ft_strcmp(params[0], "light") == 0)
		parse_light(params, rt);
	else if (ft_strcmp(params[0], "sphere") == 0)
		parse_sphere(params, rt);
	else if (ft_strcmp(params[0], "plane") == 0)
		parse_plane(params, rt);
	else if (ft_strcmp(params[0], "cylinder") == 0)
		parse_cylinder(params, rt);
	else if (ft_strcmp(params[0], "cone") == 0)
		parse_cone(params, rt);
}

static void	count_lines(int *lights, int *objects, char *name)
{
	int		fd;
	char	*line;
	char	**check;

	*lights = 0;
	*objects = 0;
	fd = open(name, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		check = ft_strsplit(line, '\t');
		if (!(ft_strcmp(check[0], "sphere")) || !(ft_strcmp(check[0], "cone"))
			|| !(ft_strcmp(check[0], "plane")) ||
			!(ft_strcmp(check[0], "cylinder")))
			(*objects)++;
		else if (!(ft_strcmp(check[0], "light")))
			(*lights)++;
		free_words(check);
		free(line);
	}
	close(fd);
}

void		parse_file(char *name, t_rt *rt)
{
	int		num_objs_lights[2];
	int		fd;
	char	*line;
	char	**split;

	parser_validation(name);
	count_lines(&num_objs_lights[1], &num_objs_lights[0], name);
	rt->objects = (t_surface *)malloc(sizeof(t_surface) *
				(num_objs_lights[0] + 1));
	rt->light_sources = (t_vec3 **)malloc(sizeof(t_vec3 *) *
						num_objs_lights[1] + 1);
	fd = open(name, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		split = ft_strsplit(line, '\t');
		select_function(rt, split);
		free(line);
	}
	rt->objects->id = FINISH;
	rt->objects = rt->objects - num_objs_lights[0];
	*(rt->light_sources) = NULL;
	rt->light_sources = rt->light_sources - num_objs_lights[1];
}
