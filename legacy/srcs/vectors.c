/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 18:35:28 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/21 19:47:35 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_vec3	vec_norm(t_vec3 *a)
{
	t_vec3	res;
	float	len;

	len = vec_len(a);
	res.x = a->x / len;
	res.y = a->y / len;
	res.z = a->z / len;
	return (res);
}

t_vec3	vec_add(t_vec3 *a, t_vec3 *b)
{
	t_vec3	res;

	res.x = a->x + b->x;
	res.y = a->y + b->y;
	res.z = a->z + b->z;
	return (res);
}

t_vec3	vec_sub(t_vec3 *a, t_vec3 *b)
{
	t_vec3	res;

	res.x = a->x - b->x;
	res.y = a->y - b->y;
	res.z = a->z - b->z;
	return (res);
}

float	vec_len(t_vec3 *a)
{
	return (sqrtf(a->x * a->x + a->y * a->y + a->z * a->z));
}
