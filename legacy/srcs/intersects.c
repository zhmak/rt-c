/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersects.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 18:40:29 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/22 14:42:48 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_solve	find_intersect_cone(t_rt *rt, t_vec3 *dir, t_vec3 *start, int i)
{
	t_vec3	point_to_tip;
	t_cone	*cone;
	float	abc[3];
	float	rad;

	cone = (t_cone *)rt->objects[i].object;
	rad = cone->angle * M_PI / 180;
	point_to_tip = vec_sub(start, &(cone->tip));
	abc[0] = powf(vec_dot_mul(dir, &(cone->axis)), 2) -
			vec_dot_mul(dir, dir) * pow(cos(rad), 2);
	abc[1] = 2 * (vec_dot_mul(dir, &(cone->axis)) *
			vec_dot_mul(&point_to_tip, &(cone->axis)) -
			vec_dot_mul(dir, &point_to_tip) * pow(cos(rad), 2));
	abc[2] = powf(vec_dot_mul(&point_to_tip, &(cone->axis)), 2) -
			vec_dot_mul(&point_to_tip, &point_to_tip) * pow(cos(rad), 2);
	return (solve_quadratic(abc));
}

t_solve	find_intersect_cylinder(t_rt *rt, t_vec3 *dir, t_vec3 *start, int i)
{
	t_vec3		cross_prod[3];
	float		abc[3];
	t_cylinder	*cylinder;

	cylinder = (t_cylinder *)(rt->objects[i].object);
	cross_prod[0] = vec_cross_mul(dir, &(cylinder->axis));
	cross_prod[1] = vec_cross_mul(start, &(cylinder->axis));
	cross_prod[2] = vec_cross_mul(&(cylinder->center), &(cylinder->axis));
	abc[0] = vec_dot_mul(&cross_prod[0], &cross_prod[0]);
	abc[1] = 2 * (vec_dot_mul(&cross_prod[1], &cross_prod[0]) -
			vec_dot_mul(&cross_prod[0], &cross_prod[2]));
	abc[2] = vec_dot_mul(&cross_prod[1], &cross_prod[1]) -
			2 * vec_dot_mul(&cross_prod[1], &cross_prod[2]) +
			vec_dot_mul(&cross_prod[2], &cross_prod[2]) -
			cylinder->radius * cylinder->radius;
	return (solve_quadratic(abc));
}

t_solve	find_intersect_plane(t_rt *rt, t_vec3 *dir, t_vec3 *start, int i)
{
	t_plane		*plane;
	t_vec3		norm_dir;
	t_solve		roots;
	float		abc[3];

	plane = (t_plane *)(rt->objects[i].object);
	norm_dir = vec_norm(dir);
	if (vec_dot_mul(&norm_dir, &(plane->normal)) == 1)
	{
		roots.root1 = INFINITY;
		roots.root2 = INFINITY;
		return (roots);
	}
	abc[0] = vec_dot_mul(&(plane->normal), &(plane->position));
	abc[1] = vec_dot_mul(&(plane->normal), start);
	abc[2] = vec_dot_mul(dir, &(plane->normal));
	roots.root1 = (abc[0] - abc[1]) / abc[2];
	roots.root2 = INFINITY;
	return (roots);
}

t_solve	find_intersect_sphere(t_rt *rt, t_vec3 *dir, t_vec3 *start, int i)
{
	float		abc[3];
	t_vec3		normal_vec;
	t_sphere	*sphere;

	sphere = (t_sphere *)(rt->objects[i].object);
	normal_vec = vec_sub(start, &(sphere->center));
	abc[0] = vec_dot_mul(dir, dir);
	abc[1] = 2.0 * vec_dot_mul(&normal_vec, dir);
	abc[2] = vec_dot_mul(&normal_vec, &normal_vec) -
		sphere->radius * sphere->radius;
	return (solve_quadratic(abc));
}
