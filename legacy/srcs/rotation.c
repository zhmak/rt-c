/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/25 23:38:43 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/17 17:47:57 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void		rotate_x(float angle, t_vec3 *ray)
{
	float rad;
	float y_local;
	float z_local;

	y_local = ray->y;
	z_local = ray->z;
	rad = M_PI / 180 * angle;
	ray->y = y_local * cos(rad) - z_local * sin(rad);
	ray->z = y_local * sin(rad) + z_local * cos(rad);
}

void		rotate_y(float angle, t_vec3 *ray)
{
	float rad;
	float x_local;
	float z_local;

	x_local = ray->x;
	z_local = ray->z;
	rad = M_PI / 180 * angle;
	ray->x = x_local * cos(rad) + z_local * sin(rad);
	ray->z = -x_local * sin(rad) + z_local * cos(rad);
}

void		rotate_z(float angle, t_vec3 *ray)
{
	float rad;
	float x_local;
	float y_local;

	x_local = ray->x;
	y_local = ray->y;
	rad = M_PI / 180 * angle;
	ray->x = x_local * cos(rad) - y_local * sin(rad);
	ray->y = x_local * sin(rad) + y_local * cos(rad);
}
