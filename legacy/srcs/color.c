/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 22:21:57 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 15:42:31 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static int	calc_end_color(int *color, int *color_buff)
{
	if (*color_buff > 0xFFFFFF)
		return (0);
	if (((*color_buff & 0xFF) + (*color & 0xFF)) <= 0xFF)
		*color += *color_buff & 0x0000FF;
	else
		*color = (*color & 0xFFFF00) + 0xFF;
	if (((*color_buff & 0xFF00) + (*color & 0xFF00)) <= 0xFF00)
		*color += *color_buff & 0xFF00;
	else
		*color = (*color & 0xFF00FF) + 0xFF00;
	if (((*color_buff & 0xFF0000) + (*color & 0xFF0000)) <= 0xFF0000)
		*color += *color_buff & 0xFF0000;
	else
		*color = (*color & 0x00FFFF) + 0xFF0000;
	return (1);
}

int			calc_shadow(t_rt *rt, t_vec3 *point_surf, int num[2])
{
	t_solve	roots;
	int		i;
	t_vec3	dir;

	i = -1;
	while (rt->objects[++i].id)
	{
		dir = vec_sub(rt->light_sources[num[1]], point_surf);
		if (rt->objects[i].id == SPHERE)
			roots = find_intersect_sphere(rt, &dir, point_surf, i);
		else if (rt->objects[i].id == PLANE)
			roots = find_intersect_plane(rt, &dir, point_surf, i);
		else if (rt->objects[i].id == CYLINDER)
			roots = find_intersect_cylinder(rt, &dir, point_surf, i);
		else if (rt->objects[i].id == CONE)
			roots = find_intersect_cone(rt, &dir, point_surf, i);
		if (roots.root1 > 0.001 && roots.root1 <= 1)
			return (0);
		if (roots.root2 > 0.001 && roots.root2 <= 1)
			return (0);
	}
	return (1);
}

int			calc_color(int obj_color, float specular, t_solve *cosines)
{
	int		colors[2];

	ft_bzero(&colors[0], sizeof(int) * 2);
	if (cosines->root1 > 0)
	{
		colors[0] += (int)((obj_color & 0x0000FF) * cosines->root1 * 0.4);
		colors[0] += (int)(((obj_color >> 8) & 0x0000FF) *
					cosines->root1 * 0.4) << 8;
		colors[0] += (int)(((obj_color >> 16) & 0x0000FF) *
					cosines->root1 * 0.4) << 16;
	}
	if (cosines->root2 > 0 && specular > 0)
	{
		colors[1] += (int)((obj_color & 0x0000FF) *
					powf(cosines->root2, specular) * 0.4);
		colors[1] += (int)(((obj_color >> 8) & 0x0000FF) *
					powf(cosines->root2, specular) * 0.4) << 8;
		colors[1] += (int)(((obj_color >> 16) & 0x0000FF) *
					powf(cosines->root2, specular) * 0.4) << 16;
	}
	return (colors[0] + colors[1]);
}

void		ambient_color(int num[2], int *color, int *color_buff, t_rt *rt)
{
	int		ambient_color;

	if (num[1] == 0)
	{
		object_color(&ambient_color, rt, num[0]);
		*color_buff += (int)((ambient_color) & 0x0000FF) * 0.2;
		*color_buff += (int)((((ambient_color) >> 8) & 0x0000FF) * 0.2) << 8;
		*color_buff += (int)((((ambient_color) >> 16) & 0x0000FF) * 0.2) << 16;
	}
	if (calc_end_color(color, color_buff) == 0)
		*color = 0xFFFFFF;
}

int			check_lights(t_rt *rt, t_vec3 *dir, float t, int i)
{
	int		color;
	int		color_buff;
	int		num[2];
	int		j;

	num[0] = i;
	j = 0;
	color = 0;
	while (rt->light_sources[j])
	{
		num[1] = j;
		if (rt->objects[i].id == SPHERE)
			color_buff = calc_light_sphere(rt, dir, t, num);
		else if (rt->objects[i].id == PLANE)
			color_buff = calc_light_plane(rt, dir, t, num);
		else if (rt->objects[i].id == CYLINDER)
			color_buff = calc_light_cylinder(rt, dir, t, num);
		else if (rt->objects[i].id == CONE)
			color_buff = calc_light_cone(rt, dir, t, num);
		ambient_color(num, &color, &color_buff, rt);
		j++;
	}
	return (color);
}
