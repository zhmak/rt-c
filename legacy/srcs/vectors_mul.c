/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors_mul.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 17:58:13 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/20 20:05:48 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_vec3	vec_num_mul(t_vec3 *a, float n)
{
	t_vec3	res;

	res.x = a->x * n;
	res.y = a->y * n;
	res.z = a->z * n;
	return (res);
}

t_vec3	vec_cross_mul(t_vec3 *a, t_vec3 *b)
{
	t_vec3	res;

	res.x = a->y * b->z - a->z * b->y;
	res.y = a->z * b->x - a->x * b->z;
	res.z = a->x * b->y - a->y * b->x;
	return (res);
}

float	vec_dot_mul(t_vec3 *a, t_vec3 *b)
{
	return (a->x * b->x + a->y * b->y + a->z * b->z);
}
