/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/19 19:23:11 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 14:49:59 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

int				calc_light_cone(t_rt *rt, t_vec3 *dir, float t, int num[2])
{
	t_vec3		surf_normal;
	t_vec3		point_surf;
	t_solve		cosines;
	t_cone		*cone;

	cosines.root1 = 0;
	cosines.root2 = 0;
	cone = (t_cone *)rt->objects[num[0]].object;
	point_surf = vec_num_mul(dir, t);
	point_surf = vec_add(&point_surf, &(rt->cam));
	if (calc_shadow(rt, &point_surf, num) == 0)
		return (calc_color(cone->color, cone->specular, &cosines));
	surf_normal = calc_normal_cone(cone, &point_surf);
	surf_normal = vec_norm(&surf_normal);
	cosines = calc_cosines(rt, &point_surf, &surf_normal, num[1]);
	if (cosines.root1 > 0 || cosines.root2 > 0)
		return (calc_color(cone->color, cone->specular, &cosines));
	return (0);
}

int				calc_light_cylinder(t_rt *rt, t_vec3 *dir, float t, int num[2])
{
	t_vec3		surf_normal;
	t_vec3		point_surf;
	t_solve		cosines;
	t_cylinder	*cylinder;

	cosines.root1 = 0;
	cosines.root2 = 0;
	cylinder = (t_cylinder *)rt->objects[num[0]].object;
	point_surf = vec_num_mul(dir, t);
	point_surf = vec_add(&point_surf, &(rt->cam));
	if (calc_shadow(rt, &point_surf, num) == 0)
		return (calc_color(cylinder->color, cylinder->specular, &cosines));
	surf_normal = calc_normal_cylinder(cylinder, &point_surf);
	surf_normal = vec_norm(&surf_normal);
	cosines = calc_cosines(rt, &point_surf, &surf_normal, num[1]);
	if (cosines.root1 > 0 || cosines.root2 > 0)
		return (calc_color(cylinder->color, cylinder->specular, &cosines));
	return (0);
}

int				calc_light_sphere(t_rt *rt, t_vec3 *dir, float t, int num[2])
{
	t_vec3		surf_normal;
	t_vec3		point_surf;
	t_solve		cosines;
	t_sphere	*sphere;

	cosines.root1 = 0;
	cosines.root2 = 0;
	sphere = (t_sphere *)rt->objects[num[0]].object;
	point_surf = vec_num_mul(dir, t);
	point_surf = vec_add(&point_surf, &(rt->cam));
	if (calc_shadow(rt, &point_surf, num) == 0)
		return (calc_color(sphere->color, sphere->specular, &cosines));
	surf_normal = vec_sub(&point_surf, &(sphere->center));
	surf_normal = vec_norm(&surf_normal);
	cosines = calc_cosines(rt, &point_surf, &surf_normal, num[1]);
	if (cosines.root1 > 0 || cosines.root2 > 0)
		return (calc_color(sphere->color, sphere->specular, &cosines));
	return (0);
}

int				calc_light_plane(t_rt *rt, t_vec3 *dir, float t, int num[2])
{
	t_vec3		surf_normal;
	t_vec3		point_surf;
	t_solve		cosines;
	t_plane		*plane;

	cosines.root1 = 0;
	cosines.root2 = 0;
	plane = (t_plane *)rt->objects[num[0]].object;
	point_surf = vec_num_mul(dir, t);
	point_surf = vec_add(&point_surf, &(rt->cam));
	if (calc_shadow(rt, &point_surf, num) == 0)
		return (calc_color(plane->color, plane->specular, &cosines));
	surf_normal = vec_norm(&(plane->normal));
	cosines = calc_cosines(rt, &point_surf, &surf_normal, num[1]);
	if (cosines.root1 > 0 || cosines.root2 > 0)
		return (calc_color(plane->color, plane->specular, &cosines));
	return (0);
}
