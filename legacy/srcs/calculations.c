/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculations.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 18:51:58 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/21 19:39:10 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_solve	solve_quadratic(float abc[3])
{
	float	discr;
	t_solve roots;

	discr = abc[1] * abc[1] - 4.0 * abc[0] * abc[2];
	if (discr < 0)
	{
		roots.root1 = INFINITY;
		roots.root2 = INFINITY;
		return (roots);
	}
	roots.root1 = (-abc[1] + sqrtf(discr)) / (2.0 * abc[0]);
	roots.root2 = (-abc[1] - sqrtf(discr)) / (2.0 * abc[0]);
	return (roots);
}

t_solve	calc_cosines(t_rt *rt, t_vec3 *point_surf,
					t_vec3 *surf_normal, int light)
{
	t_vec3		point_to_light;
	t_vec3		point_to_cam;
	t_vec3		bisector;
	t_solve		cosines;

	point_to_light = vec_sub(rt->light_sources[light], point_surf);
	point_to_light = vec_norm(&point_to_light);
	point_to_cam = vec_sub(&(rt->cam), point_surf);
	point_to_cam = vec_norm(&point_to_cam);
	bisector = vec_add(&point_to_cam, &point_to_light);
	bisector = vec_norm(&bisector);
	cosines.root1 = vec_dot_mul(surf_normal, &point_to_light);
	cosines.root2 = vec_dot_mul(surf_normal, &bisector);
	return (cosines);
}

t_vec3	calc_normal_cone(t_cone *cone, t_vec3 *point_surf)
{
	float		tip_to_point_len;
	float		tip_to_vertex_len;
	t_vec3		tip_to_point;
	t_vec3		tip_offset;
	t_vec3		vertex;

	tip_to_point = vec_sub(point_surf, &(cone->tip));
	tip_to_point_len = vec_len(&tip_to_point);
	tip_to_vertex_len = tip_to_point_len * cos(cone->angle * M_PI / 180);
	tip_to_point = vec_norm(&tip_to_point);
	if (vec_dot_mul(&(cone->axis), &tip_to_point) < 0)
		tip_offset = vec_num_mul(&(cone->axis), -tip_to_vertex_len);
	else
		tip_offset = vec_num_mul(&(cone->axis), tip_to_vertex_len);
	vertex = vec_add(&(cone->tip), &(tip_offset));
	return (vec_sub(point_surf, (&vertex)));
}

t_vec3	calc_normal_cylinder(t_cylinder *cylinder, t_vec3 *point_surf)
{
	float		center_to_point_len;
	float		cosine;
	t_vec3		new_center;
	t_vec3		center_to_point;
	t_vec3		center_offset;

	center_to_point = vec_sub(point_surf, &(cylinder->center));
	center_to_point_len = vec_len(&center_to_point);
	center_to_point = vec_norm(&center_to_point);
	cosine = vec_dot_mul((&cylinder->axis), &center_to_point);
	center_to_point_len = center_to_point_len * cosine;
	center_offset = vec_num_mul(&(cylinder->axis), center_to_point_len);
	new_center = vec_add(&(cylinder->center), &(center_offset));
	return (vec_sub(point_surf, (&new_center)));
}
