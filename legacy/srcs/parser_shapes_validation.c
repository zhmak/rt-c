/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_shapes_validation.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/24 12:13:30 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 15:52:06 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	check_floats(char **param, int i, char *name)
{
	char	**num;
	int		j;

	j = 0;
	while (j < i)
	{
		num = ft_strsplit(param[j++], '.');
		check_param_num(num, 2, name);
		free_words(num);
	}
}

void	validate_sphere(char **params)
{
	char		**split;

	check_param_num(params, 6, "sphere");
	check_floats(&params[4], 1, "sphere");
	check_floats(&params[2], 1, "sphere");
	split = ft_strsplit(params[1], ',');
	check_param_num(split, 3, "sphere");
	check_floats(split, 3, "sphere");
	free_words(split);
	free_words(params);
}

void	validate_cylinder(char **params)
{
	char		**split;

	check_param_num(params, 7, "cylinder");
	check_floats(&params[3], 1, "cylinder");
	check_floats(&params[5], 1, "cylinder");
	split = ft_strsplit(params[1], ',');
	check_param_num(split, 3, "cylinder");
	check_floats(split, 3, "cylinder");
	free_words(split);
	split = ft_strsplit(params[2], ',');
	check_param_num(split, 3, "cylinder");
	check_floats(split, 3, "cylinder");
	free_words(split);
	free_words(params);
}

void	validate_cone(char **params)
{
	char	**split;

	check_param_num(params, 7, "cone");
	check_floats(&params[3], 1, "cone");
	check_floats(&params[5], 1, "cone");
	split = ft_strsplit(params[1], ',');
	check_param_num(split, 3, "cone");
	check_floats(split, 3, "cone");
	free_words(split);
	split = ft_strsplit(params[2], ',');
	check_param_num(split, 3, "cone");
	check_floats(split, 3, "cone");
	free_words(split);
	free_words(params);
}

void	validate_plane(char **params)
{
	char	**split;

	check_param_num(params, 6, "plane");
	check_floats(&params[4], 1, "plane");
	split = ft_strsplit(params[1], ',');
	check_param_num(split, 3, "plane");
	check_floats(split, 3, "plane");
	free_words(split);
	split = ft_strsplit(params[2], ',');
	check_param_num(split, 3, "plane");
	check_floats(split, 3, "plane");
	free_words(split);
	free_words(params);
}
