/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 14:57:52 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 15:51:19 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void	check_argument(int argc, char *argv)
{
	int fd;

	if (argc != 2)
	{
		ft_putstr("usage: rt_scene_file\n");
		exit(0);
	}
	fd = open(argv, O_RDWR);
	if (fd < 0)
	{
		ft_putstr("Invalid file. Exiting.\n");
		exit(0);
	}
	close(fd);
}

static void	init_mlx(t_rt *rt)
{
	rt->mlx.mlx_ptr = mlx_init();
	rt->mlx.win_ptr = mlx_new_window(rt->mlx.mlx_ptr, SIZE_W, SIZE_H, "RT");
	rt->mlx.img_ptr = mlx_new_image(rt->mlx.mlx_ptr, SIZE_W, SIZE_H);
	rt->mlx.image = (int *)(mlx_get_data_addr(rt->mlx.img_ptr, &(rt->mlx.bits),
					&(rt->mlx.colms), &(rt->mlx.endian)));
	trace_ray(rt);
	mlx_put_image_to_window(rt->mlx.mlx_ptr, rt->mlx.win_ptr,
							rt->mlx.img_ptr, 0, 0);
	mlx_hook(rt->mlx.win_ptr, 2, 0, key_press, rt);
	mlx_hook(rt->mlx.win_ptr, 12, 0, expose, rt);
	mlx_hook(rt->mlx.win_ptr, 17, 0, close_window, rt);
	mlx_loop(rt->mlx.mlx_ptr);
}

void		calc_basis(t_rt *rt)
{
	rt->basis.w = (t_vec3){0, 0, -1};
	if (rt->view_port.angle_y != 0)
		rotate_y(rt->view_port.angle_y, &(rt->basis.w));
	if (rt->view_port.angle_x != 0)
		rotate_x(rt->view_port.angle_x, &(rt->basis.w));
	rt->basis.u = vec_cross_mul(&(rt->basis.w), &(t_vec3){0, 1, 0});
	rt->basis.v = vec_cross_mul(&(rt->basis.u), &(rt->basis.w));
}

static void	init_val(t_rt *rt)
{
	rt->cam = (t_vec3){0, 0, 0};
	rt->view_port.width = 2;
	rt->view_port.height = 2;
	rt->view_port.dist = 2;
}

int			main(int argc, char **argv)
{
	t_rt	rt;

	check_argument(argc, argv[1]);
	init_val(&rt);
	parse_file(argv[1], &rt);
	calc_basis(&rt);
	init_mlx(&rt);
	(void)argc;
	(void)argv;
	return (0);
}
