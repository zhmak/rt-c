/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_shapes.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 17:41:16 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 15:18:33 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	parse_sphere(char **params, t_rt *rt)
{
	char		**split;
	t_sphere	*sphere;

	sphere = (t_sphere *)malloc(sizeof(t_sphere));
	sphere->specular = ft_atof(params[4]);
	sphere->radius = ft_atof(params[2]);
	sphere->color = ft_atoi_16(params[3]);
	split = ft_strsplit(params[1], ',');
	sphere->center.x = ft_atof(split[0]);
	sphere->center.y = ft_atof(split[1]);
	sphere->center.z = ft_atof(split[2]);
	free_words(split);
	free_words(params);
	rt->objects->id = SPHERE;
	rt->objects->object = sphere;
	rt->objects++;
}

void	parse_cylinder(char **params, t_rt *rt)
{
	char		**split;
	t_cylinder	*cylinder;

	cylinder = (t_cylinder *)malloc(sizeof(t_cylinder));
	cylinder->specular = ft_atof(params[5]);
	cylinder->radius = ft_atof(params[3]);
	cylinder->color = ft_atoi_16(params[4]);
	split = ft_strsplit(params[1], ',');
	cylinder->center.x = ft_atof(split[0]);
	cylinder->center.y = ft_atof(split[1]);
	cylinder->center.z = ft_atof(split[2]);
	free_words(split);
	split = ft_strsplit(params[2], ',');
	cylinder->axis.x = ft_atof(split[0]);
	cylinder->axis.y = ft_atof(split[1]);
	cylinder->axis.z = ft_atof(split[2]);
	cylinder->axis = vec_norm(&(cylinder->axis));
	free_words(split);
	free_words(params);
	rt->objects->id = CYLINDER;
	rt->objects->object = cylinder;
	rt->objects++;
}

void	parse_cone(char **params, t_rt *rt)
{
	char	**split;
	t_cone	*cone;

	cone = (t_cone *)malloc(sizeof(t_cone));
	cone->specular = ft_atof(params[5]);
	cone->angle = ft_atof(params[3]);
	cone->color = ft_atoi_16(params[4]);
	split = ft_strsplit(params[1], ',');
	cone->tip.x = ft_atof(split[0]);
	cone->tip.y = ft_atof(split[1]);
	cone->tip.z = ft_atof(split[2]);
	free_words(split);
	split = ft_strsplit(params[2], ',');
	cone->axis.x = ft_atof(split[0]);
	cone->axis.y = ft_atof(split[1]);
	cone->axis.z = ft_atof(split[2]);
	cone->axis = vec_norm(&(cone->axis));
	free_words(split);
	free_words(params);
	rt->objects->id = CONE;
	rt->objects->object = cone;
	rt->objects++;
}

void	parse_plane(char **params, t_rt *rt)
{
	char	**split;
	t_plane	*plane;

	plane = (t_plane *)malloc(sizeof(t_plane));
	plane->specular = ft_atof(params[4]);
	plane->color = ft_atoi_16(params[3]);
	split = ft_strsplit(params[1], ',');
	plane->normal.x = ft_atof(split[0]);
	plane->normal.y = ft_atof(split[1]);
	plane->normal.z = ft_atof(split[2]);
	plane->normal = vec_norm(&(plane->normal));
	free_words(split);
	split = ft_strsplit(params[2], ',');
	plane->position.x = ft_atof(split[0]);
	plane->position.y = ft_atof(split[1]);
	plane->position.z = ft_atof(split[2]);
	free_words(split);
	free_words(params);
	rt->objects->id = PLANE;
	rt->objects->object = plane;
	rt->objects++;
}
