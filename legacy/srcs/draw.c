/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 15:46:30 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/21 19:47:17 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static t_solve	choose_object(t_rt *rt, t_vec3 *dir, t_vec3 *start, int i)
{
	t_solve roots;

	roots = (t_solve){INFINITY, INFINITY};
	if (rt->objects[i].id == SPHERE)
		roots = find_intersect_sphere(rt, dir, start, i);
	else if (rt->objects[i].id == PLANE)
		roots = find_intersect_plane(rt, dir, start, i);
	else if (rt->objects[i].id == CYLINDER)
		roots = find_intersect_cylinder(rt, dir, start, i);
	else if (rt->objects[i].id == CONE)
		roots = find_intersect_cone(rt, dir, start, i);
	return (roots);
}

static int		check_ray(t_rt *rt, t_vec3 *dir)
{
	t_solve	roots;
	float	min_dist;
	int		object;
	int		i;

	min_dist = INFINITY;
	object = -1;
	i = -1;
	while (rt->objects[++i].id)
	{
		roots = choose_object(rt, dir, &(rt->cam), i);
		if (roots.root1 > rt->view_port.dist && roots.root1 < min_dist)
		{
			min_dist = roots.root1;
			object = i;
		}
		if (roots.root2 > rt->view_port.dist && roots.root2 < min_dist)
		{
			min_dist = roots.root2;
			object = i;
		}
	}
	if (object == -1)
		return (0x1283ec);
	return (check_lights(rt, dir, min_dist, object));
}

void			trace_ray(t_rt *rt)
{
	int		i;
	int		j;
	t_vec3	dir;
	t_basis	temp;

	temp.w = vec_num_mul(&(rt->basis.w), -(rt->view_port.dist));
	i = 0;
	while (i < SIZE_H)
	{
		j = 0;
		temp.v = vec_num_mul(&(rt->basis.v), -((i * rt->view_port.height /
				(SIZE_H - 1) - rt->view_port.height / 2)));
		while (j < SIZE_W)
		{
			temp.u = vec_num_mul(&(rt->basis.u), ((float)j *
					rt->view_port.width / (SIZE_W - 1) -
					rt->view_port.width / 2.0));
			dir = vec_add(&temp.w, &temp.v);
			dir = vec_add(&dir, &temp.u);
			dir = vec_norm(&dir);
			rt->mlx.image[j + i * SIZE_W] = check_ray(rt, &dir);
			j++;
		}
		i++;
	}
}
