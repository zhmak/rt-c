/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 16:58:48 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/20 20:18:51 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void		change_dir(int keycode, t_rt *rt)
{
	if (keycode == 88)
		rt->view_port.angle_y += 10;
	if (keycode == 86)
		rt->view_port.angle_y -= 10;
	if (keycode == 91)
		rt->view_port.angle_x -= 10;
	if (keycode == 84)
		rt->view_port.angle_x += 10;
	if (keycode == 87)
	{
		rt->view_port.angle_x = 0;
		rt->view_port.angle_y = 0;
	}
}

static void		change_cam2(int keycode, t_rt *rt)
{
	t_vec3 buff;

	if (keycode == 258)
	{
		buff = vec_num_mul(&(rt->basis.v), 0.5);
		rt->cam = vec_add(&(rt->cam), &buff);
	}
	else if (keycode == 269)
	{
		buff = vec_num_mul(&(rt->basis.v), -0.5);
		rt->cam = vec_add(&(rt->cam), &buff);
	}
	else if (keycode == 49)
	{
		rt->cam.x = 0;
		rt->cam.y = 0;
		rt->cam.z = 0;
	}
}

static void		change_cam(int keycode, t_rt *rt)
{
	t_vec3 buff;

	if (keycode == 126)
	{
		buff = vec_num_mul(&(rt->basis.w), -0.5);
		rt->cam = vec_add(&(rt->cam), &buff);
	}
	else if (keycode == 125)
	{
		buff = vec_num_mul(&(rt->basis.w), 0.5);
		rt->cam = vec_add(&(rt->cam), &buff);
	}
	else if (keycode == 124)
	{
		buff = vec_num_mul(&(rt->basis.u), 0.5);
		rt->cam = vec_add(&(rt->cam), &buff);
	}
	if (keycode == 123)
	{
		buff = vec_num_mul(&(rt->basis.u), -0.5);
		rt->cam = vec_add(&(rt->cam), &buff);
	}
	else
		change_cam2(keycode, rt);
}

int				close_window(void *param)
{
	(void)param;
	exit(0);
	return (0);
}

int				key_press(int keycode, t_rt *rt)
{
	if (keycode == 69)
		rt->view_port.dist += 0.1;
	if (keycode == 78)
		rt->view_port.dist -= 0.1;
	if (keycode == 53)
		exit(0);
	change_dir(keycode, rt);
	change_cam(keycode, rt);
	calc_basis(rt);
	trace_ray(rt);
	mlx_clear_window(rt->mlx.mlx_ptr, rt->mlx.win_ptr);
	mlx_put_image_to_window(rt->mlx.mlx_ptr, rt->mlx.win_ptr,
							rt->mlx.img_ptr, 0, 0);
	return (0);
}
