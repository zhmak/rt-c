/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_validation.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 14:38:08 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 15:52:51 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void		check_param_num(char **param, int i, char *name)
{
	int		j;

	j = 0;
	while (*param)
	{
		param++;
		j++;
	}
	if (i == j)
		return ;
	ft_putstr("Wrong ");
	ft_putstr(name);
	ft_putstr(" arguments. Exiting.\n");
	exit(0);
}

static void	validate_light(char **params)
{
	char	**coords;

	check_param_num(params, 3, "light");
	coords = ft_strsplit(params[1], ',');
	check_floats(coords, 3, "light");
	check_param_num(coords, 3, "light");
	free_words(coords);
	free_words(params);
}

static void	validate_cam(char **params)
{
	char	**coords;

	check_param_num(params, 4, "camera");
	coords = ft_strsplit(params[1], ',');
	check_param_num(coords, 3, "camera");
	check_floats(coords, 3, "camera");
	free_words(coords);
	coords = ft_strsplit(params[2], ',');
	check_param_num(coords, 2, "camera");
	check_floats(coords, 2, "camera");
	free_words(coords);
	free(params);
}

static void	select_function(char **params)
{
	if (ft_strcmp(params[0], "camera") == 0)
		validate_cam(params);
	else if (ft_strcmp(params[0], "light") == 0)
		validate_light(params);
	else if (ft_strcmp(params[0], "sphere") == 0)
		validate_sphere(params);
	else if (ft_strcmp(params[0], "plane") == 0)
		validate_plane(params);
	else if (ft_strcmp(params[0], "cylinder") == 0)
		validate_cylinder(params);
	else if (ft_strcmp(params[0], "cone") == 0)
		validate_cone(params);
	else
	{
		ft_putstr("Wrong object name. Exiting.\n");
		exit(0);
	}
}

void		parser_validation(char *name)
{
	int		fd;
	char	*line;
	char	**split;

	fd = open(name, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		split = ft_strsplit(line, '\t');
		select_function(split);
		free(line);
	}
}
