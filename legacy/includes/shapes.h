/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shapes.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 16:24:57 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 14:44:43 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHAPES_H
# define SHAPES_H

# include "util.h"

# define FINISH		0
# define SPHERE		1
# define PLANE		2
# define CYLINDER	3
# define CONE		4

typedef struct	s_surface
{
	char		id;
	void		*object;
}				t_surface;

typedef struct	s_sphere
{
	t_vec3		center;
	float		radius;
	float		specular;
	int			color;
}				t_sphere;

typedef struct	s_plane
{
	t_vec3		normal;
	t_vec3		position;
	float		specular;
	int			color;
}				t_plane;

typedef struct	s_cylinder
{
	t_vec3		center;
	t_vec3		axis;
	float		radius;
	float		specular;
	int			color;
}				t_cylinder;

typedef struct	s_cone
{
	t_vec3		tip;
	t_vec3		axis;
	float		angle;
	float		specular;
	int			color;
}				t_cone;

#endif
