/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 20:27:40 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/20 20:13:32 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTIL_H
# define UTIL_H

typedef struct	s_vec3
{
	float		x;
	float		y;
	float		z;
}				t_vec3;

typedef struct	s_basis
{
	t_vec3		u;
	t_vec3		v;
	t_vec3		w;
}				t_basis;

typedef struct	s_solve
{
	float		root1;
	float		root2;
}				t_solve;

#endif
