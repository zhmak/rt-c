/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 14:58:12 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/26 14:48:39 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H

# include "util.h"
# include "shapes.h"
# include "mlx.h"
# include "libft.h"
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <math.h>

# define SIZE_W 512
# define SIZE_H 512
# define PHONG_EXP 50

typedef struct	s_mlx
{
	void		*mlx_ptr;
	void		*win_ptr;
	void		*img_ptr;
	int			*image;
	int			bits;
	int			colms;
	int			endian;
}				t_mlx;

typedef struct	s_vp
{
	float		width;
	float		height;
	float		dist;
	float		angle_x;
	float		angle_y;
}				t_vp;

typedef struct	s_rt
{
	t_mlx		mlx;
	t_vec3		cam;
	t_vp		view_port;
	t_vec3		**light_sources;
	t_surface	*objects;
	t_basis		basis;
}				t_rt;

t_solve			solve_quadratic(float abc[3]);
t_solve			calc_cosines(t_rt *rt, t_vec3 *point_surf,
							t_vec3 *surf_normal, int light);
t_vec3			calc_normal_cylinder(t_cylinder *cylinder, t_vec3 *point_surf);
t_vec3			calc_normal_cone(t_cone *cone, t_vec3 *point_surf);

int				calc_light_sphere(t_rt *rt, t_vec3 *dir, float t, int num[2]);
int				calc_light_plane(t_rt *rt, t_vec3 *dir, float t, int num[2]);
int				calc_light_cylinder(t_rt *rt, t_vec3 *dir, float t, int num[2]);
int				calc_light_cone(t_rt *rt, t_vec3 *dir, float t, int num[2]);

t_solve			find_intersect_sphere(t_rt *rt, t_vec3 *dir,
									t_vec3 *start, int i);
t_solve			find_intersect_plane(t_rt *rt, t_vec3 *dir,
									t_vec3 *start, int i);
t_solve			find_intersect_cylinder(t_rt *rt, t_vec3 *dir,
									t_vec3 *start, int i);
t_solve			find_intersect_cone(t_rt *rt, t_vec3 *dir,
									t_vec3 *start, int i);

void			calc_basis(t_rt *rt);
void			trace_ray(t_rt *rt);
int				check_lights(t_rt *rt, t_vec3 *dir, float t, int i);
int				calc_shadow (t_rt *rt, t_vec3 *point_surf, int num[2]);
int				calc_color(int obj_color, float specular, t_solve *cosines);
void			object_color(int *color, t_rt *rt, int num);

int				close_window(void *param);
int				key_press(int keycode, t_rt *rt);
int				expose(t_rt *rt);

t_vec3			vec_cross_mul(t_vec3 *a, t_vec3 *b);
t_vec3			vec_num_mul(t_vec3 *a, float n);
t_vec3			vec_norm(t_vec3 *a);
t_vec3			vec_sub(t_vec3 *a, t_vec3 *b);
t_vec3			vec_add(t_vec3 *a, t_vec3 *b);
float			vec_dot_mul(t_vec3 *a, t_vec3 *b);
float			vec_len(t_vec3 *a);

void			rotate_x(float angle, t_vec3 *ray);
void			rotate_y(float angle, t_vec3 *ray);
void			rotate_z(float angle, t_vec3 *ray);

void			parse_file(char *name, t_rt *rt);
void			parse_sphere(char **params, t_rt *rt);
void			parse_cylinder(char **params, t_rt *rt);
void			parse_cone(char **params, t_rt *rt);
void			parse_plane(char **params, t_rt *rt);

void			parser_validation(char *name);
void			validate_cone(char **params);
void			validate_cylinder(char **params);
void			validate_sphere(char **params);
void			validate_plane(char **params);

void			check_param_num(char **param, int i, char *name);
void			check_floats(char **param, int i, char *name);

#endif
